<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::match(['get', 'post'], '/analysis', 'AnalysisController@index')->name('analis');

Route::post('/login', 'Auth\LoginController@apiLogin');

Route::post('/change_password/get_sms', 'Auth\ResetPasswordController@getSms');

Route::post('/change_password/check_sms', 'Auth\ResetPasswordController@checkSms');

Route::middleware('auth:api')->post('/change_password', 'Auth\ResetPasswordController@changePassword');

// Route::get('/patient/information/{unp}', 'PatientController@index')->name('patient');

// http://medlk:81/api/patient/information/NDU=