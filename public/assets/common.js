document.addEventListener('DOMContentLoaded', () => {

	const form = document.getElementById('forTest');

	if (form) {
		form.addEventListener('submit', async (e) => {

			e.preventDefault();

			const res = await fetch(form.action, {
				method: "POST",
				body: new FormData(form)
			});
			
			if (res.ok) {
				const json = await res.json();
				document.querySelector('code').innerHTML = JSON.stringify(json);
			} else {
				document.querySelector('code').innerHTML = `Плохой запрос! Статус - ${res.status}`;
			}

		});
	}

	const patientEl = document.querySelector('#patient_id');

	if (patientEl) {
		
		try {
			const patients = JSON.parse(patientEl.dataset.patients);

			const observer = new MutationObserver((mutation) => {
				let patientData;

				if (patientData = patients.find(patient => patient.id == mutation[0].target.value)) {
					document.getElementById('patient__unp').value = patientData.unp;
					document.getElementById('patient__user__dob').value = patientData.dob;
				}
			});
			
			observer.observe(patientEl, {
				attributes: true,
				attributeFilter: ['value'],
				attributeOldValue: true,
			});

		} catch (error) {
			console.error(error, 'Popal');
		}

	}
	
});