<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddOptionsToAnalyzesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('analyzes', function (Blueprint $table) {
            $table->string("option_time")->nullable();
            $table->string("option_сhecked")->nullable();
            $table->string("option_wbc")->nullable();
            $table->string("option_flag_wbc")->nullable();
            $table->string("option_lymph_h")->nullable();
            $table->string("option_flag_lymph_h")->nullable();
            $table->string("option_mid_h")->nullable();
            $table->string("option_flag_mid_h")->nullable();
            $table->string("option_gran_h")->nullable();
            $table->string("option_flag_gran_h")->nullable();
            $table->string("option_lymph_p")->nullable();
            $table->string("option_flag_lymph_p")->nullable();
            $table->string("option_mid_p")->nullable();
            $table->string("option_flag_mid_p")->nullable();
            $table->string("option_gran_p")->nullable();
            $table->string("option_flag_gran_p")->nullable();
            $table->string("option_rbc")->nullable();
            $table->string("option_flag_rbc")->nullable();
            $table->string("option_hgb")->nullable();
            $table->string("option_flag_hgb")->nullable();
            $table->string("option_hct")->nullable();
            $table->string("option_flag_hct")->nullable();
            $table->string("option_mcv")->nullable();
            $table->string("option_flag_mcv")->nullable();
            $table->string("option_mch")->nullable();
            $table->string("option_flag_mch")->nullable();
            $table->string("option_mchc")->nullable();
            $table->string("option_flag_mchc")->nullable();
            $table->string("option_rdw_cv")->nullable();
            $table->string("option_flag_rdw_cv")->nullable();
            $table->string("option_rdw_sd")->nullable();
            $table->string("option_flag_rdw_sd")->nullable();
            $table->string("option_plt")->nullable();
            $table->string("option_flag_plt")->nullable();
            $table->string("option_mpv")->nullable();
            $table->string("option_flag_mpv")->nullable();
            $table->string("option_pdw")->nullable();
            $table->string("option_flag_pdw")->nullable();
            $table->string("option_pct")->nullable();
            $table->string("option_flag_pct")->nullable();
            $table->string("option_p_lcc")->nullable();
            $table->string("option_flag_p_lcc")->nullable();
            $table->string("option_p_lcr")->nullable();
            $table->string("option_flag_p_lcr")->nullable();
            $table->string("option_mode")->nullable();
            $table->string("option_ref_diapaz")->nullable();
            $table->string("option_sex")->nullable();
            $table->string("option_age")->nullable();
            $table->string("option_branch")->nullable();
            $table->string("option_place")->nullable();
            $table->string("option_date_sampling")->nullable();
            $table->string("option_time_selection")->nullable();
            $table->string("option_date_delivery")->nullable();
            $table->string("option_time_delivery")->nullable();
            $table->string("option_submit")->nullable();
            $table->string("option_analyzer")->nullable();
            $table->string("option_comments")->nullable();
            $table->string("option_flag_histogram_wbc")->nullable();
            $table->string("option_flag_histogram_plt")->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('analyzes', function (Blueprint $table) {
            //
        });
    }
}
