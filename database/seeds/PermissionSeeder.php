<?php

use Illuminate\Database\Seeder;
use App\Permission;

class PermissionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $permission = new Permission();
        $permission->name = "Удалить";
        $permission->slug = "remove";
        $permission->save();

        $permission = new Permission();
        $permission->name = "Создать";
        $permission->slug = "create";
        $permission->save();

        // ===
        $permission = new Permission();
        $permission->name = "Создать исследования";
        $permission->slug = "create_analyzes";
        $permission->save();

        $permission = new Permission();
        $permission->name = "Удалить исследования";
        $permission->slug = "remove_analyzes";
        $permission->save();
        
        $permission = new Permission();
        $permission->name = "Читать исследования";
        $permission->slug = "view_analyzes";
        $permission->save();

        $permission = new Permission();
        $permission->name = "Редактировать исследования";
        $permission->slug = "edit_analyzes";
        $permission->save();

        // ===
        $permission = new Permission();
        $permission->name = "Создать пациентов";
        $permission->slug = "create_patients";
        $permission->save();

        $permission = new Permission();
        $permission->name = "Удалить пациентов";
        $permission->slug = "remove_patients";
        $permission->save();

        $permission = new Permission();
        $permission->name = "Читать пациентов";
        $permission->slug = "view_patients";
        $permission->save();

        $permission = new Permission();
        $permission->name = "Редактировать пациентов";
        $permission->slug = "edit_patients";
        $permission->save();

        // ===
        $permission = new Permission();
        $permission->name = "Создать пользователей";
        $permission->slug = "create_users";
        $permission->save();

        $permission = new Permission();
        $permission->name = "Удалить пользователей";
        $permission->slug = "remove_users";
        $permission->save();

        $permission = new Permission();
        $permission->name = "Читать пользователей";
        $permission->slug = "view_users";
        $permission->save();

        $permission = new Permission();
        $permission->name = "Редактировать пользователей";
        $permission->slug = "edit_users";
        $permission->save();
    }
}
