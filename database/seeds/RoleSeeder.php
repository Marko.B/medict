<?php

use App\Role;
use App\Permission;
use Illuminate\Database\Seeder;

class RoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
       // Доктор (может просматривать вкладку «пациенты», просматривать и вкладку «анализы» (с возможностью редактирования мета поля “комментарий врача”);
       $role = Role::find(5);
       $role->permissions()->saveMany([
           Permission::find(13),
           Permission::find(9),
       ]);
    }
}
