<?php

use App\User;
use App\Role;
use App\Patient;
use Illuminate\Support\Facades\Hash;
use Illuminate\Database\Seeder;
use Illuminate\Support\Str;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $users = User::find([41, 40, 39]);

        foreach ($users as $user) {
            $user->roles()->save(Role::find(2));
        }
    }
}
