<?php

/** TODO:
 * 1. Создать роли: Главная, Главврач, Доктор, Лаборант, Юрист. + 
 * 2. Добавить поле даты рождения и блокировки учетной записи. +
 * 3. Создать права на доступ к модулям (секциям админ панели).
 */


namespace App\Providers;

use SleepingOwl\Admin\Providers\AdminSectionsServiceProvider as ServiceProvider;

class AdminSectionsServiceProvider extends ServiceProvider
{
    /**
     * @var array
     */
    protected $sections = [
        \App\User::class => \App\Http\Admin\Users::class,
        \App\Patient::class => \App\Http\Admin\Patients::class,
        \App\Analysis::class => \App\Http\Admin\Analyzes::class,
    ];

    protected $policies = [
        \App\Http\Admin\Users::class => \App\Policies\Admin\UserPolicy::class,
        \App\Http\Admin\Patients::class => \App\Policies\Admin\PatientPolicy::class,
        \App\Http\Admin\Analyzes::class => \App\Policies\Admin\AnalysisPolicy::class,
    ];

    private function registerRoutes()
    {
        $this->app['router']->group([
            'prefix' => config('sleeping_owl.url_prefix'),
            'middleware' => config('sleeping_owl.middleware')],
            function($router) {
                $router->get('/create_pdf', ['as' => 'admin.pdf', function(){
                    return 'New PDF';
                }]);
            }
        );
    }

    /**
     * Register sections.
     *
     * @param \SleepingOwl\Admin\Admin $admin
     * @return void
     */
    public function boot(\SleepingOwl\Admin\Admin $admin)
    {
        $this->registerPolicies();

        parent::boot($admin);

        $this->registerRoutes();
    }
}
