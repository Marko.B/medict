<?php

namespace App\Policies\Admin;

use App\User;
use App\Policies\Admin\MainPolicy;
use Illuminate\Auth\Access\HandlesAuthorization;

class PatientPolicy extends MainPolicy
{
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }
}
