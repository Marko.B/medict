<?php

/**
 * TODO:
 * 1. Удалить поля full_name и dob из таблицы пациент
 * 2. Настроить вывод полей в админки
 */

namespace App\Http\Admin;

use App\User;
use App\Role;
use AdminColumn;
use AdminColumnFilter;
use AdminDisplay;
use AdminForm;
use AdminFormElement;
use Illuminate\Database\Eloquent\Model;
use SleepingOwl\Admin\Contracts\Display\DisplayInterface;
use SleepingOwl\Admin\Contracts\Form\FormInterface;
use SleepingOwl\Admin\Contracts\Initializable;
use SleepingOwl\Admin\Form\Buttons\Cancel;
use SleepingOwl\Admin\Form\Buttons\Save;
use SleepingOwl\Admin\Form\Buttons\SaveAndClose;
use SleepingOwl\Admin\Form\Buttons\SaveAndCreate;
use SleepingOwl\Admin\Section;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

/**
 * Class Patients
 *
 * @property \App\Patient $model
 *
 * @see https://sleepingowladmin.ru/#/ru/model_configuration_section
 */
class Patients extends Section implements Initializable
{
    /**
     * @var bool
     */
    protected $checkAccess = true;

    /**
     * @var string
     */
    protected $title = 'Пациенты';

    /**
     * @var string
     */
    protected $alias;

    /**
     * Initialize class.
     */
    public function initialize()
    {
        $this->addToNavigation()->setPriority(101)->setIcon('fas fa-user-injured');

        $this->deleting([$this, 'handleDeleting']);

        $this->created([$this, 'handleSave']);
    }

    public function handleSave($config, $model)
    {   
        $model->user->roles()->save(Role::find(2));
    }

    public function handleDeleting($config, $model)
    {
        $model->user->delete();
        return false;
    }

    public function hasPermission($field)
    {
        if (auth()->user()->roles->contains('slug', 'doctor')) {
            return $field->setReadonly(true);
        }

        return $field;
    }

    /**
     * @param array $payload
     *
     * @return DisplayInterface
     */
    public function onDisplay($payload = [])
    {
        $columns = [
            AdminColumn::text('unp', 'УНП'),
            AdminColumn::link('user.name', 'ФИО', 'create_at'),
        ];

        $display = AdminDisplay::datatables()
            ->setName('patientsdatatables')
            ->setOrder([[0, 'asc']])
            ->paginate(30)
            ->setColumns($columns)
            ->setHtmlAttribute('class', 'table-primary table-hover');

        $display->setColumnFilters([
            AdminColumnFilter::text('custom_id', 'УНП')->setPlaceholder('УНП'),
            AdminColumnFilter::text('full_name')->setPlaceholder('ФИО')->setOperator('begins_with'),
        ]);

        $display->getColumnFilters()->setPlacement('card.heading');

        return $display;
    }

    /**
     * @param int|null $id
     * @param array $payload
     *
     * @return FormInterface
     */
    public function onEdit($id = null, $payload = [])
    {
        $patient = [
            AdminFormElement::text('unp', 'УНП')
                ->required()
                ->unique(),
            AdminFormElement::text('user.name', 'ФИО')
                ->required(),
            AdminFormElement::text('user.email', 'Адрес электронной почты')
                ->unique()
                ->required()
                ->addValidationRule('email:rfc,dns'),
            AdminFormElement::date('user.dob', 'Дата рождения')
                ->required(),
            AdminFormElement::number('user.phone', 'Номер телефона')
                ->unique()
                ->required()
                ->addValidationRule('digits:11'),
            AdminFormElement::password('user.password', 'Пароль')
                ->hashWithBcrypt()
                ->setValidationRules(['required', 'min:8']),
            AdminFormElement::password('password_confirmation', 'Подтвердить пароль')
                ->required()
                ->setValueSkipped(true)
                ->addValidationRule('same:user.password', 'Подтвердить'),
        ];

        $patient = array_map([$this, 'hasPermission'], $patient);

        $analyzes = [
            AdminFormElement::view('admin.patients.table', [
                'analyzes' => $this->model->find($id)->analyzes->toArray(),
            ]),
        ];

        $tabs = AdminDisplay::tabbed()
            ->setTabs(function ($id) use ($patient, $analyzes) {
                return [
                    AdminDisplay::tab(AdminForm::elements($patient))->setLabel('Пациент'),
                    AdminDisplay::tab(AdminForm::elements($analyzes))->setLabel('Исследования'),
                ];
            });

        $form = AdminForm::form()
            ->addElement($tabs);

        $form->getButtons()->setButtons([
            'save'  => new Save(),
            'save_and_close'  => new SaveAndClose(),
            'cancel'  => (new Cancel()),
        ]);

        return $form;
    }

    /**
     * @return FormInterface
     */
    public function onCreate($payload = [])
    {
        $fields = [
            AdminFormElement::text('unp', 'УНП')
                ->required()
                ->unique(),
            AdminFormElement::text('user.name', 'ФИО')
                ->required(),
            AdminFormElement::text('user.email', 'Адрес электронной почты')
                ->setValidationRules(['required', 'email:rfc,dns', 'unique:App\User,email']),
            AdminFormElement::date('user.dob', 'Дата рождения')
                ->required(),
            AdminFormElement::number('user.phone', 'Номер телефона')
                ->setValidationRules(['digits:11', 'required', 'unique:App\User,phone', 'numeric']),
            AdminFormElement::password('user.password', 'Пароль')
                ->hashWithBcrypt()
                ->setValidationRules(['required', 'min:8']),
            AdminFormElement::password('password_confirmation', 'Подтвердить пароль')
                ->required()
                ->setValueSkipped(true)
                ->addValidationRule('same:user.password', 'Подтвердить'),
            AdminFormElement::hidden('user.api_token')->setDefaultValue(Str::random(80)),
        ];

        $form = AdminForm::card()->addBody($fields);

        $form->getButtons()->setButtons([
            'save_and_close'  => new SaveAndClose(),
            'cancel'  => (new Cancel()),
        ]);

        return $form;
    }

    public function onDelete($id)
    {
        // todo: remove if unused
    }

    /**
     * @return void
     */
    public function onRestore($id)
    {
        // remove if unused
    }
}
