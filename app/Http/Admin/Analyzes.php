<?php

namespace App\Http\Admin;

use AdminColumn;
use AdminColumnFilter;
use AdminDisplay;
use AdminForm;
use AdminFormElement;
use Illuminate\Database\Eloquent\Model;
use SleepingOwl\Admin\Contracts\Display\DisplayInterface;
use SleepingOwl\Admin\Contracts\Form\FormInterface;
use SleepingOwl\Admin\Contracts\Initializable;
use SleepingOwl\Admin\Form\Buttons\Cancel;
use SleepingOwl\Admin\Form\Buttons\Save;
use SleepingOwl\Admin\Form\Buttons\SaveAndClose;
use SleepingOwl\Admin\Form\Buttons\SaveAndCreate;
use SleepingOwl\Admin\Display\ControlButton;
use SleepingOwl\Admin\Section;
use App\Patient;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Config;
use App\User;

/**
 * Class Analyzes
 *
 * @property \App\Analysis $model
 *
 * @see https://sleepingowladmin.ru/#/ru/model_configuration_section
 */
class Analyzes extends Section implements Initializable
{
    /**
     * @var bool
     */
    protected $checkAccess = true;

    /**
     * @var string
     */
    protected $title = 'Исследования';

    /**
     * @var string
     */
    protected $alias = 'analyzes';

    /**
     * Initialize class.
     */
    public function initialize()
    {
        $this->addToNavigation()->setPriority(100)->setIcon('fas fa-microscope');
    }

    /**
     * @param array $payload
     *
     * @return DisplayInterface
     */
    public function onDisplay($payload = [])
    {
        $columns = [
            AdminColumn::link('una', 'УНИ'),

            AdminColumn::custom('ФИО', function($model) {
                return "<a href='/admin/patients/{$model->patient->id}/edit'>{$model->patient->user->name}</a>";
            }),

            AdminColumn::datetime('date', 'Дата сдачи')
                ->setFormat('d.m.Y'),
        ];

        $filters = [
            AdminColumnFilter::text('una', 'УНИ')->setPlaceholder('УНИ'),

            AdminColumnFilter::text()->setPlaceholder('ФИО')->setOperator('begins_with'),

            AdminColumnFilter::range()->setFrom(
                AdminColumnFilter::date()
                    ->setPlaceholder('От')
                    ->setPickerFormat('d.m.Y')
            )->setTo(
                AdminColumnFilter::date()
                    ->setPlaceholder('До')
                    ->setPickerFormat('d.m.Y')
            ),
        ];

        $display = AdminDisplay::datatables()
            ->setName('analyzesdatatables')
            ->setOrder([[0, 'asc']])
            ->paginate(30)
            ->setColumns($columns)
            ->setHtmlAttribute('class', 'table-primary table-hover');

        $display->setColumnFilters($filters);

        $display->getColumnFilters()->setPlacement('card.heading');

        $control = $display->getColumns()->getControlColumn();

        $button = new ControlButton(function () {
            $url = route('admin.pdf');
            return $url;
        }, 'сгенирировать PDF');

        $button
            ->hideText()
            ->setMethod('get')
            ->setIcon('fas fa-file-pdf')
            ->setHtmlAttributes([
                'class' => 'btn-primary',
                'style' => 'width: 26px',
            ]);

        $control->addButton($button);

        return $display;
    }

    public function hasPermission($field)
    {
        if (auth()->user()->roles->contains('slug', 'doctor')) {
            return $field->setReadonly(function ($model) use ($field) {
                if ($field->getName() == 'comment_doctor') {
                    return false;
                }
                return true;
            });
        }

        return $field;
    }

    /**
     * @param int|null $id
     * @param array $payload
     *
     * @return FormInterface
     */
    public function onEdit($id = null, $payload = [])
    {
        $patients = [];

        foreach (Patient::all() as $patient) {
            $patients[] = [
                'id' => $patient->id,
                'unp' => $patient->unp,
                'dob' => $patient->user->dob,
            ];
        }

        $patients = json_encode($patients);

        $fields = [
            AdminFormElement::text('una', 'УНИ')
                ->required()
                ->unique(),
            AdminFormElement::select('patient_id', 'ФИО')
                ->required()
                ->setHtmlAttribute('data-patients', $patients)
                ->setModelForOptions(\App\Patient::class, 'user.name'),
            AdminFormElement::date('date', 'Дата сдачи')->required(),
            AdminFormElement::time('time', 'Время сдачи')->required(),
            AdminFormElement::text('patient.unp', 'УНП Пациента')->setReadOnly(true),
            AdminFormElement::text('patient.user.dob', 'Дата рождения пациента')->setReadOnly(true),
            AdminFormElement::textarea('comment_doctor', 'Комментарий врача'),
        ];

        if ($options = Config::get('constants.options_analyzes')) {
            if ($cols = DB::select('select * from INFORMATION_SCHEMA.COLUMNS where table_name = "analyzes"')) {
                foreach ($cols as $col) {
                    if (isset($options[$col->COLUMN_NAME])) {
                        $fields[] = AdminFormElement::text($col->COLUMN_NAME, $options[$col->COLUMN_NAME]);
                    }
                }
            }
        }

        $fields = array_map([$this, 'hasPermission'], $fields);

        $fields[] = AdminFormElement::html("<div class='alert alert-warning'>Исследование с таким номером еще не добавлено в систему</div>");

        $form = AdminForm::card()
            ->addBody($fields)
            ->addScript('custom-js', '/assets/common.js');

        $form->getButtons()->setButtons([
            'save'  => new Save(),
            'save_and_close'  => new SaveAndClose(),
            'cancel'  => (new Cancel()),
        ]);

        return $form;
    }

    /**
     * @return FormInterface
     */
    public function onCreate($payload = [])
    {
        $patients = [];

        foreach (Patient::all() as $patient) {
            $patients[] = [
                'id' => $patient->id,
                'unp' => $patient->unp,
                'dob' => $patient->user->dob,
            ];
        }

        $patients = json_encode($patients);

        $fields = [
            AdminFormElement::text('una', 'УНИ')
                ->required()
                ->unique(),
            AdminFormElement::select('patient_id', 'ФИО')
                ->required()
                ->setHtmlAttribute('data-patients', $patients)
                ->setModelForOptions(\App\Patient::class, 'user.name'),
            AdminFormElement::date('date', 'Дата сдачи')->required(),
            AdminFormElement::time('time', 'Время сдачи')->required(),
            AdminFormElement::text('patient.unp', 'УНП Пациента')->setReadOnly(true),
            AdminFormElement::text('patient.user.dob', 'Дата рождения пациента')->setReadOnly(true),
            AdminFormElement::textarea('comment_doctor', 'Комментарий врача'),
        ];

        if ($options = Config::get('constants.options_analyzes')) {
            if ($cols = DB::select('select * from INFORMATION_SCHEMA.COLUMNS where table_name = "analyzes"')) {
                foreach ($cols as $col) {
                    if (isset($options[$col->COLUMN_NAME])) {
                        $fields[] = AdminFormElement::text($col->COLUMN_NAME, $options[$col->COLUMN_NAME]);
                    }
                }
            }
        }

        $form = AdminForm::card()
            ->addBody($fields)
            ->addScript('custom-js', '/assets/common.js');

        $form->getButtons()->setButtons([
            'save_and_close'  => new SaveAndClose(),
            'cancel'  => (new Cancel()),
        ]);

        return $form;
    }

    public function onDelete($id)
    {
        // todo: remove if unused
    }

    /**
     * @return bool
     */

    /**
     * @return void
     */
    public function onRestore($id)
    {
        // remove if unused
    }
}
