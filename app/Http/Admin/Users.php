<?php

namespace App\Http\Admin;

use AdminColumn;
use AdminColumnFilter;
use AdminDisplay;
use AdminForm;
use AdminFormElement;
use Illuminate\Database\Eloquent\Model;
use SleepingOwl\Admin\Contracts\Display\DisplayInterface;
use SleepingOwl\Admin\Contracts\Form\FormInterface;
use SleepingOwl\Admin\Contracts\Initializable;
use SleepingOwl\Admin\Form\Buttons\Cancel;
use SleepingOwl\Admin\Form\Buttons\Save;
use SleepingOwl\Admin\Form\Buttons\SaveAndClose;
use SleepingOwl\Admin\Form\Buttons\SaveAndCreate;
use SleepingOwl\Admin\Section;
use Illuminate\Support\Str;

/**
 * Class Users
 *
 * @property \App\User $model
 *
 * @see https://sleepingowladmin.ru/#/ru/model_configuration_section
 */
class Users extends Section implements Initializable
{
    /**
     * @var bool
     */
    protected $checkAccess = true;

    /**
     * @var string
     */
    protected $title = 'Пользователи';

    /**
     * @var string
     */
    protected $alias;

    /**
     * Initialize class.
     */
    public function initialize()
    {
        $this->addToNavigation()->setPriority(102)->setIcon('fas fa-users');
    }

    /**
     * @param array $payload
     *
     * @return DisplayInterface
     */
    public function onDisplay($payload = [])
    {
        $columns = [
            AdminColumn::link('name', 'Имя пользователя', 'created_at'),
            AdminColumn::text('email', 'Email'),
            AdminColumn::lists('roles.name', 'Роль'),
        ];

        $display = AdminDisplay::datatables()
            ->setName('firstdatatables')
            ->setOrder([[0, 'asc']])
            ->paginate(30)
            ->setApply([
                function ($query) {
                    $query->whereNotIn('id', function ($subquery) {
                        $subquery->select('user_id')->from('patients');
                    });
                }
            ])
            ->setColumns($columns)
            ->setHtmlAttribute('class', 'table-primary table-hover');

        return $display;
    }

    /**
     * @param int|null $id
     * @param array $payload
     *
     * @return FormInterface
     */
    public function onEdit($id = null, $payload = [])
    {
        $fields = [
            AdminFormElement::text('name', 'ФИО')
                ->required(),
            AdminFormElement::text('email', 'адрес электронной почты')
                ->unique()
                ->required()
                ->addValidationRule('email:rfc,dns'),
            AdminFormElement::date('dob', 'Дата рождения')
                ->required(),
            AdminFormElement::number('phone', 'Номер телефона')
                ->unique()
                ->required()
                ->addValidationRule('digits:11'),
            AdminFormElement::multiselect('roles', 'Роль')
                ->setModelForOptions(\App\Role::class, 'name')
                ->required(),
            AdminFormElement::password('password', 'Пароль')
                ->hashWithBcrypt()
                ->setValidationRules(['required', 'min:8']),
            AdminFormElement::password('password_confirmation', 'Подтвердить пароль')
                ->required()
                ->setValueSkipped(true)
                ->addValidationRule('same:password', 'Подтвердить'),
            AdminFormElement::checkbox('is_blocked', 'Блокировать'),
        ];

        $form = AdminForm::card()->addBody($fields);

        $form->getButtons()->setButtons([
            'save'  => new Save(),
            'save_and_close'  => new SaveAndClose(),
            'cancel'  => (new Cancel()),
        ]);

        return $form;
    }

    /**
     * @return FormInterface
     */
    public function onCreate($payload = [])
    {
        $fields = [
            AdminFormElement::text('name', 'ФИО')
                ->required(),
            AdminFormElement::text('email', 'адрес электронной почты')
                ->unique()
                ->required()
                ->addValidationRule('email:rfc,dns'),
            AdminFormElement::date('dob', 'Дата рождения')
                ->required(),
            AdminFormElement::number('phone', 'Номер телефона')
                ->unique()
                ->required()
                ->addValidationRule('digits:11'),
            AdminFormElement::multiselect('roles', 'Роль')
                ->setModelForOptions(\App\Role::class, 'name')
                ->required(),
            AdminFormElement::password('password', 'Пароль')
                ->hashWithBcrypt()
                ->setValidationRules(['required', 'min:8']),
            AdminFormElement::password('password_confirmation', 'Подтвердить пароль')
                ->required()
                ->setValueSkipped(true)
                ->addValidationRule('same:password', 'Подтвердить'),
            AdminFormElement::checkbox('is_blocked', 'Блокировать'),
            AdminFormElement::hidden('api_token')->setDefaultValue(Str::random(80)),
        ];

        $form = AdminForm::card()->addBody($fields);

        $form->getButtons()->setButtons([
            'save_and_close'  => new SaveAndClose(),
            'cancel'  => (new Cancel()),
        ]);

        return $form;
    }

    public function onDelete($id)
    {
        // todo: remove if unused
    }

    /**
     * @return void
     */
    public function onRestore($id)
    {
        // remove if unused
    }
}
