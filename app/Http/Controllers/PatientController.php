<?php

namespace App\Http\Controllers;

use App\Patient;
use Illuminate\Http\Request;

class PatientController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * @OA\Get(
     *   path="/api/patient/information/{unp}",
     *   tags={"patient"},
     *   summary="Получить информацию о пациенте",
     *   operationId="getPatient",
     * 
     *   @OA\Parameter(
     *      name="unp",
     *      in="path",
     *      description="Введите УНП пациента в формате Base64",
     *      required=true,
     *      @OA\Schema(
     *        type="string",
     *        format="byte"
     *      )
     *   ),
     * 
     *   @OA\Response(
     *      response=200,
     *      description="Успех",  
     *      @OA\MediaType(
     *          mediaType="application/json"
     *      )
     *   ),
     * 
     *  @OA\Response(
     *      response=400,
     *      description="Плохой запрос",  
     *  ),
     * 
     *  @OA\Response(
     *      response=401,
     *      description="Неавторизованный пользователь",  
     *  ),
     * 
     * @OA\Response(
     *      response=404,
     *      description="Ничего не найдено",  
     * ),
     * 
     *  @OA\SecurityScheme(
     *      type="apiKey",
     *      in="header",
     *      securityScheme="api_key",
     *      name="api_key"
     *  ),
     * 
     *  security={
     *      {"api_key": {}}
     *  },
     * 
     * )
     */

    public function index(Request $request, $unp)
    {
        if (!($request->user()->hasRole('patient'))) {
            abort(400, 'Bad request1');
        }

        if ($unp = base64_decode($unp)) {
            $data = [];

            if ($patient = Patient::where('unp', $unp)->first()) {
                $data['unp'] = $patient->unp;
                $data['full_name'] = $patient->user->name;
                $data['dob'] = $patient->user->dob;
                $data['email'] = $patient->user->email;
                $data['phone'] = $patient->user->phone;

                if ($analyzes = $patient->analyzes) {
                    foreach ($analyzes  as $analysis) {
                        $data['analyzes'][] = $analysis->toArray();
                    }
                }
            } else {
                return response()->json($data, 404);
            }

            return response()->json($data);
        } else {
            abort(400, 'Bad request2');
        }
    }
}
