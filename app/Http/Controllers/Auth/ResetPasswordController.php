<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
// use Composer\DependencyResolver\Request;
use Illuminate\Http\Request;
use Illuminate\Foundation\Auth\ResetsPasswords;
use Illuminate\Support\Facades\Validator;
use App\User;
use Illuminate\Support\Facades\Hash;
// use Nexmo\Laravel\Facade\Nexmo;

class ResetPasswordController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset requests
    | and uses a simple trait to include this behavior. You're free to
    | explore this trait and override any methods you wish to tweak.
    |
    */

    use ResetsPasswords;

    /**
     * Where to redirect users after resetting their password.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;

    private $apiID = "7FBE383E-7ED1-4EBE-1BBE-7CF0EDCD4D6B";

    const API_URL = "https://sms.ru/sms";

    /**
     * @OA\Post(
     *   path="/api/change_password/check_sms",
     *   tags={"reset"},
     *   summary="Сменить пароль",
     *   operationId="checkSms",
     *  
     *   @OA\RequestBody(
     *      description="Укажите код из смс",
     *      @OA\JsonContent(
     *        required={"sms"},    
     *        @OA\Property(
     *          property="sms", 
     *          type="string",
     *        ),
     *      )
     *   ),
     * 
     *   @OA\Response(
     *     response=200,
     *     description="Успех",
     *     @OA\MediaType(
     *          mediaType="application/json"
     *     )
     *   ), 
     * 
     * @OA\Response(
     *     response=400,
     *     description="Пожалуйста, проверьте правильность написания кода",
     *     @OA\MediaType(
     *          mediaType="application/json"
     *     )
     *   ), 
     * 
     * @OA\Response(
     *     response=404,
     *     description="Код не сгенерирован",
     *     @OA\MediaType(
     *          mediaType="application/json"
     *     )
     *   ),
     * 
     * )
     */
    public function checkSms(Request $request)
    {
        if ($request->cookie('code_sms') && $request->has('sms')) {

            $data = json_decode($request->cookie('code_sms'));

            if (Hash::check($request->input('sms'), $data[1])) {

                if ($user = User::find($data[0])) {
                    return response()->json(['api_token' => $user->api_token]);
                }

                return response()->json(['message' => 'Ошибка!'], 400);
            }

            return response()->json(['message' =>  "Не верный код !"], 400);
        }

        return response()->json(['message' => 'Код не сгенерирован'], 404);
    }

    //xYESzTSsSN3Gjtx4bVj93P8dfcUHupUEVOHM9vpgGq81sSjppyuCoHTLoAvwWIm8k9Ccmgdw4veBbKiH
    // 89859640565

    /**
     * @OA\Post(
     *   path="/api/change_password",
     *   tags={"reset"},
     *   summary="Сменить пароль",
     *   operationId="changePassword",
     * 
     *   @OA\RequestBody(
     *      description="Введите пароль",
     *      @OA\JsonContent(
     *        required={"password1, password2, api_token"},    
     *        @OA\Property(
     *          property="password1", 
     *          type="string",
     *        ),
     *        @OA\Property(
     *          property="password2", 
     *          type="string",
     *        ),
     *        @OA\Property(
     *          property="api_token", 
     *          type="string",
     *        ),
     *      )
     *   ),
     * 
     *   security={
     *      {"api_key": {}}
     *   },
     *  
     * @OA\Response(
     *     response=200,
     *     description="Успех",
     *     @OA\MediaType(
     *          mediaType="application/json"
     *     )
     *   ), 
     * 
     *  @OA\Response(
     *     response=400,
     *     description="Пожалуйста, проверьте правильность написания пароля",
     *     @OA\MediaType(
     *          mediaType="application/json"
     *     )
     *   ), 
     * 
     *  @OA\Response(
     *     response=404,
     *     description="Не найдено",
     *     @OA\MediaType(
     *          mediaType="application/json"
     *     )
     *   ), 
     * 
     * )
     */
    public function changePassword(Request $request)
    {
        if ($user = User::where('api_token', $request->input('api_token'))->first()) {

            if ($request->filled(['password1', 'password2'])) {

                $validator = Validator::make(['password1' => $request->input('password1')], [
                    'password1' => ['min:8'],
                ]);

                if ($validator->fails()) {
                    return response()->json([
                        'status' => 'error',
                        'message' => $validator->errors()
                    ], 400);
                }

                if ($request->input('password1') == $request->input('password2')) {
                    $user->password = Hash::make($request->input('password1'));
                    $user->save();
                    return response()->json(['status' => 'ok']);
                }

                return response()->json([
                    'status' => 'error',
                    'message' => 'Пожалуйста, проверьте правильность написания пароля'
                ], 400);
            }

            return response()->json([
                'status' => 'error',
                'message' => 'Пожалуйста, проверьте правильность написания пароля'
            ], 400);
        }

        return response()->json([
            'status' => 'error',
            'message' => 'Ошибка'
        ], 404);
    }

    /**
     * @OA\Post(
     *   path="/api/change_password/get_sms",
     *   tags={"reset"},
     *   summary="Сменить пароль",
     *   operationId="getSms",
     * 
     *   @OA\RequestBody(
     *      description="Укажите телефон",
     *      @OA\JsonContent(
     *        required={"phone"},    
     *        @OA\Property(
     *          property="phone", 
     *          type="string",
     *        ),
     *      )
     *   ),
     * 
     *   @OA\Response(
     *     response=200,
     *     description="Успех",
     *     @OA\MediaType(
     *          mediaType="application/json"
     *     )
     *   ),  
     *   @OA\Response(
     *     response=400,
     *     description="Пожалуйста, проверьте правильность написания логина",
     *     @OA\MediaType(
     *          mediaType="application/json"
     *     )
     *   ),  
     * 
     * )
     */
    public function getSms(Request $request)
    {
        if ($request->has('phone')) {

            $validator = Validator::make(['phone' => $request->input('phone')], [
                'phone' => ['digits:11'],
            ]);

            if ($validator->fails()) {
                return response()->json([
                    'status' => 'error',
                    'message' => $validator->errors()
                ], 400);
            }

            if ($user = User::where('phone', $request->input('phone'))->first()) {

                if ($request->cookie("code_sms")) {
                    return response()->json(['message' => 'Повторный запрос возможен не раньше чем через 60 секунд !'], 400);
                } else {
                    $code = rand(0, 9) . rand(0, 9) . rand(0, 9) . rand(0, 9);

                    $args = http_build_query([
                        'json' => 1,
                        'test' => 1,
                        'to' => $user->phone,
                        'api_id' => $this->apiID,
                        // 'msg' => urlencode("Code{$code}"),
                        'msg' => "Code: {$code}",
                    ]);

                    try {
                        $res = file_get_contents(self::API_URL . "/send?{$args}");
                        $data = json_decode($res);
                        // return response()->json(['tmp' => self::API_URL . "/send?{$args}"]);

                        if ($data->status == 'OK') {
                            // return response()->json($data, 200);
                            return response()
                                ->json(['code' => $code, 'api' => $data], 200)
                                ->cookie("code_sms", json_encode([$user->id, Hash::make($code)]), 1);
                        } else {
                            return response()
                                ->json($data, 400);
                        }
                    } catch (Exception $e) {
                        return response()
                            ->json([
                                'status' => 'error',
                                'message' => $e,
                            ], 400);
                    }
                }
            }

            return response()->json([
                'status' => 'error',
                'message' => $request->input('phone') . " - номер не зарегистрирован в системе!"
            ], 400);
        }

        return response()->json([
            'status' => 'error',
            'message' => "Заполните поля",
        ], 400);
    }
}
