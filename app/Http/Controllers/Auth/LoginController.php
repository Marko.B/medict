<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Http\RedirectResponse;
use App\User;
use Illuminate\Support\Facades\Hash;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    public function credentials(Request $request)
    {
        if (is_numeric($request->get('email'))) {
            return ['phone' => $request->get('email'), 'password' => $request->get('password')];
        } else {
            return ['email' => $request->get('email'), 'password' => $request->get('password')];
        }
    }

    protected function authenticated(Request $request, $user)
    {
        if ($user->hasRole('main', 'admin', 'chief-physician', 'doctor', 'assistant', 'lawyer')) {
            return redirect('admin');
        }

        return redirect('/');
    }

    /**
     * @OA\Post(
     *   path="/api/login",
     *   tags={"auth"},
     *   summary="Получить токен пользователя",
     *   operationId="getToken",
     *   
     *   @OA\RequestBody(
     *      description="Укажите телефон и пароль",
     *      @OA\JsonContent(
     *        required={"una, dob"},    
     *        @OA\Property(
     *          property="phone", 
     *          type="string",
     *          format="byte",
     *        ),
     *        @OA\Property(
     *          property="password", 
     *          type="string",
     *          format="byte",
     *        )
     *      )
     *   ),
     * 
     *  @OA\Response(
     *     response=200,
     *     description="Успех",
     *     @OA\MediaType(
     *          mediaType="application/json"
     *     )
     *   ),
     *  @OA\Response(
     *     response=404,
     *     description="Пользователь не найден",
     *     @OA\MediaType(
     *          mediaType="application/json"
     *     )
     *  ),
     *  @OA\Response(
     *     response=400,
     *     description="Плохой запрос",
     *     @OA\MediaType(
     *          mediaType="application/json"
     *     )
     *  ),
     * 
     * )
     */
    public function apiLogin(Request $request)
    {
        if ($request->has(['phone', 'password'])) {
            
            if ($user = User::where('phone', base64_decode($request->input('phone')))->first()) {
                
                if (Hash::check(base64_decode($request->input('password')), $user->password)) {
                    return response()->json([
                        'status' => 'ok',
                        'api_token' => $user->api_token, 
                    ], 200);
                } else {
                    return response()->json([
                        'status' => 'error',
                        'message' => 'Пожалуйста, проверьте правильность написания логина и пароля.',
                    ], 404); 
                }

            } else {
                return response()->json([
                    'status' => 'error',
                    'message' => 'Пожалуйста, проверьте правильность написания логина и пароля.',
                ], 404);
            }
        }

        return response()->json([
            'status' => 'error',
            'message' => 'Заполните все поля',
        ], 400);
    }
}

// {
//     "phone": 32423423423,MzI0MjM0MjM0MjM=
//     "password": "larisalarisa"bGFyaXNhbGFyaXNh
//   }
