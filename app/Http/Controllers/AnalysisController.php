<?php

namespace App\Http\Controllers;

use App\Analysis;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;

class AnalysisController extends Controller
{
    /**
     * @OA\Post(
     *   path="/api/analysis",
     *   tags={"analysis"},
     *   summary="Получить объект исследования",
     *   operationId="getAnalysis",
     * 
     *   @OA\RequestBody(
     *      description="Укажите УНИ и дату рождения пациента",
     *      @OA\JsonContent(
     *        required={"una, dob"},    
     *        @OA\Property(
     *          property="una", 
     *          type="string",
     *        ),
     *        @OA\Property(
     *          property="dob", 
     *          type="string",
     *          format="date"
     *        )
     *      )
     *   ),
     * 
     *   @OA\Response(
     *     response=200,
     *     description="Успех",
     *     @OA\MediaType(
     *          mediaType="application/json"
     *     )
     *   ),
     *   @OA\Response(
     *      response=404,
     *      description="Ничего не найдено",
     *      @OA\MediaType(
     *          mediaType="application/json"
     *     )
     *   ),
     *   @OA\Response(
     *      response=400,
     *      description="Плохой запрос",
     *      @OA\MediaType(
     *          mediaType="application/json"
     *     )
     *   ),
     *   
     * )
     */
    public function index(Request $request)
    {
        $data = [];

        $status = 200;

        if ($request->isMethod('post')) {

            if (!($request->filled('una') && $request->filled('dob'))) {
                $status = 400;
                $data['message'] = 'Заполните все поля';
            } else {
                $validator = Validator::make($request->all(), [
                    'dob' => 'date',
                ]);

                if ($validator->fails()) {
                    return response()->json(['message' => 'Дата в некорректном формате!'], 400);
                }
               
                $date = strtotime($request->input('dob'));
                
                $date = date('Y-m-d', $date);

                $data = Analysis::where([
                    ['una', '=', $request->input('una')],
                    [function($query) {
                        $query->select('dob')
                            ->from('users')
                            ->whereExists(function($subquery) {
                                $subquery->select(DB::raw(1))
                                    ->from('patients')
                                    ->whereRaw('patients.user_id = users.id');
                            })
                            ->limit(1);
                    }, '=', $date],
                ])->get();

                if (sizeof($data) == 0) {
                    $status = 404;
                }
            }

        } else {
            $status = 400;
            $data['message'] = 'Запрос должен отправляться методом post';
        }

        return response()->json($data, $status);
    }
}
