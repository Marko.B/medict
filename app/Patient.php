<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Patient extends Model
{
    public function analyzes()
    {
        return $this->hasMany('App\Analysis');
    }

    public function user()
    {
        return $this->belongsTo('App\User');
    }
}
