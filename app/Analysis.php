<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Analysis extends Model
{
    protected $table = 'analyzes';

    protected $fillable = ['una', 'date', 'time', 'comment_doctor', 'patient_id'];

    public function patient()
    {
        return $this->belongsTo('App\Patient');    
    }
}
