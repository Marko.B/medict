@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-lg-4">
                <form id="forTest" method="POST" action="{{ route('analis') }}">
                    <div class="form-group">
                        <label for="exampleInputEmail1">УНИ</label>
                        <input name="una" type="text" class="form-control" id="exampleInputEmail1">
                    </div>
                    <div class="form-group">
                        <label for="exampleInputPassword1">Дата рождения пациента</label>
                        {{-- <input name="dob" type="date" class="form-control"> --}}
                        <input name="dob" type="text" placeholder="в формате 03-01-1992" class="form-control">
                    </div>
                    <button type="submit" class="btn btn-primary">Click</button>
                </form>
            </div>
            <div class="col-lg-8">
                <code>[{'message': 'Hello'}]</code>
            </div>
        </div>
    </div>
@endsection
