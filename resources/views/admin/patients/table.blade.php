@if ($analyzes)
    <table class="table">
        <thead>
            <tr>
                <th scope="col">УНИ</th>
                <th scope="col">Дата</th>
                <th scope="col">Действия</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($analyzes as $analysis)
                <tr>
                    <th scope="row">{{ $analysis['una'] }}</th>
                    <td>{{ $analysis['date'] }}</td>
                    <td>
						<a class="btn btn-sm btn-primary" href="{{ route('admin.model.edit', ['adminModel' => 'analyzes', 'adminModelId' => $analysis['id']]) }}">Редактировать</a>
						<a class="btn btn-sm btn-primary" href="{{ route('admin.pdf') }}">Сгенерировать</a>
					</td>
                </tr>
            @endforeach
        </tbody>
    </table>
@endif
